using UnityEngine;
using UnityEngine.UI;

public class BackPanel : MonoBehaviour
{
    public MainMenu ui;
    public Button backButton;

	void Start()
	{
        backButton.onClick.AddListener(() => ui.SwitchTo(Panel.Main));
	}
	
}
