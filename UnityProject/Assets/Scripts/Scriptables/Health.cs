using UnityEngine;

[CreateAssetMenu(fileName = "Health", menuName = "Health")]
public class Health : ScriptableObject
{
    public float value = 100;

    [SerializeField]
    private float defaultValue;

    private void OnDisable()
    {
        value = defaultValue;
    }
}
