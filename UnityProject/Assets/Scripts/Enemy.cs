using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour, IBreakable
{
    public float health = 1;
    public float speed = 10;
    private float stun = 0;
    public float shootCooldown = 2;
    private bool died = false;

    [SerializeField]
    private GameObject attack;
    [SerializeField]
    private Transform gun;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private Transform[] waypoints;
    private int currentWPIndex = 0;
    [SerializeField]
    private Transform sightDistance;
    [SerializeField]
    private Transform eyes;
    [SerializeField]
    private LayerMask sightMask;
    [SerializeField]
    private Sensor sensor;
    private bool seePlayer = false;

    private AnimationController anim;
    private Rigidbody2D rigid;
    private Gravity gravity;
    private CheckCollision col;
    private int groundCheckAfter = 2;
    public ParticleSystem blood;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        anim = GetComponent<AnimationController>();
        col = GetComponentInChildren<CheckCollision>();
        col.hitTheGround.AddListener(HitTheGroundEvent);
        gravity = GetComponent<Gravity>();
        attack.SetActive(false);
    }

    private void HitTheGroundEvent()
    {
        gravity.applyGravity = false;
        Vector2 vel = rigid.velocity;
        vel.y = 0;
        rigid.velocity = vel;
    }

    private void Attack()
    {
        StartCoroutine(AttackCoroutine(0.5f));
    }

    IEnumerator AttackCoroutine(float time)
    {
        if (attack.activeSelf || stun > 0) yield break;
        shootCooldown = 2;
        anim.Hit();
        attack.SetActive(true);
        yield return new WaitForSeconds(time);
        attack.SetActive(false);
    }

    public void Hit(float damage, Vector3 contactPoint, Vector3 normal)
    {
        blood.gameObject.transform.position = contactPoint;
        blood.gameObject.transform.up = normal;
        blood.Play();
        if (died) return;
        health -= damage;
        if (health <= 0)
        {
            died = true;
            anim.Die();
            StartCoroutine(Die());
            EventManager.enemyDied.Invoke(this);
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
    }

    public void Shoot()
    {
        if (shootCooldown <= 0 && !attack.activeSelf && sensor.player)
        {
            anim.Shoot();
            shootCooldown = 2;
            Bullet blt = Instantiate(bullet, gun.position, Quaternion.identity).GetComponent<Bullet>();
            blt.ChangeDirection((sensor.player.transform.position - gun.position + Vector3.up / 2).normalized);
        }
    }

    private void Update()
    {
        gravity.applyGravity = !col.onTheGround;
        if (died && col.onTheGround)
        {
            rigid.velocity = Vector3.zero;
            return;
        }
        if (Input.GetKeyDown(KeyCode.RightControl))
            Shoot();

        Sight();
        Move();
        if (shootCooldown > 0)
            shootCooldown -= Time.deltaTime;
    }

    private void Sight()
    {
        RaycastHit2D hit;
        ContactFilter2D cf = new ContactFilter2D();
        cf.SetLayerMask(sightMask);
        if (sensor.player)
        {
            hit = Physics2D.Raycast(eyes.position, sensor.player.transform.position - eyes.position + Vector3.up / 2, 5, sightMask);
            if (hit && hit.collider.gameObject == sensor.player)
            {
                if (hit.distance < 0.5f)
                    Attack();
                seePlayer = true;
            }
            else
                seePlayer = false;
        }
        else
            seePlayer = false;
    }

    void Move()
    {
        Vector3 vel = rigid.velocity;
        if (stun <= 0 && !seePlayer)
        {
            if (waypoints != null && waypoints.Length > 1)
            {
                if (Mathf.Abs(waypoints[currentWPIndex].position.x - transform.position.x) > 0.1f)
                    vel.x = waypoints[currentWPIndex].position.x - transform.position.x < 0 ? -1 : 1;
                else
                    GetWPIndex();
            }
        }
        else if (!seePlayer)
        {
            if (groundCheckAfter <= 0 && col.onTheGround)
                vel.x = 0;
            else
                groundCheckAfter--;
            stun -= Time.deltaTime;
        }
        else if (seePlayer)
        {
            if (col.onTheGround)
                vel.x = 0;
            Shoot();
        }
        rigid.velocity = vel;
        anim.SetSpeed(rigid.velocity);
    }

    private void GetWPIndex()
    {
        if (waypoints != null && waypoints.Length > 0)
        {
            if (++currentWPIndex >= waypoints.Length)
                currentWPIndex = 0;
        }
    }

    public void KickBack(Vector3 dir, float stunTime)
    {
        groundCheckAfter = 2;
        rigid.AddForce(dir, ForceMode2D.Impulse);
        stun = stunTime;
    }
}
