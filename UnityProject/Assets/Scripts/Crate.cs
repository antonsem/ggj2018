using UnityEngine;

public class Crate : MonoBehaviour
{
    private CheckCollision ground;
    private Gravity gravity;

    void Start()
    {
        ground = GetComponentInChildren<CheckCollision>();
        gravity = GetComponent<Gravity>();
    }

    void Update()
    {
        gravity.applyGravity = !ground.onTheGround;
    }
}
