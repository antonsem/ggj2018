using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField]
    private Collider2D col;
    public Vector3 kickForce = new Vector3(2, 2, 0);
    public float hitDamage = 25;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IBreakable breakable = collision.GetComponent<IBreakable>();
        if (breakable != null)
        {
            breakable.Hit(hitDamage, transform.position, transform.right);
            breakable.KickBack(new Vector3(kickForce.x * transform.right.x, kickForce.y, 0), 2);
        }
        else
        {
            Bullet blt = collision.GetComponent<Bullet>();
            if (blt)
                blt.ReverseDirection();
        }
    }   

}
