using UnityEngine;
using UnityEngine.Events;

public class HitTheGround : UnityEvent { }
public class HitEvent : UnityEvent { }
public class EnemyDied : UnityEvent<Enemy> { }

public static class EventManager
{
    public static EnemyDied enemyDied = new EnemyDied();
}
