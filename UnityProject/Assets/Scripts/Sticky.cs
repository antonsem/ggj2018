using UnityEngine;

public class Sticky : MonoBehaviour
{
    public FrictionJoint2D fricJ;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.transform.SetParent(transform);
        Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
        if (fricJ && rb)
            fricJ.connectedBody = rb;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.transform.SetParent(null);
        if (fricJ)
            fricJ.connectedBody = null;
    }
}
