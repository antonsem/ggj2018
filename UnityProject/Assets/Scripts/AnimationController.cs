using Spine.Unity;
using UnityEngine;

public class AnimationController : MonoBehaviour
{

    [Header("Spine"), SerializeField]
    private SkeletonAnimation skeleton;
    [SpineAnimation] public string run, idle, shoot, jump, fall, stunned, die, hit;
    Spine.Bone bone;
    [SpineBone, SerializeField]
    private string aimBone;

    private void Start()
    {
        bone = skeleton.Skeleton.FindBone(aimBone);
        //skeleton.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        Vector3 localPositon = transform.InverseTransformPoint(Input.mousePosition);
        localPositon /= 1000;
        bone.SetPositionSkeletonSpace(localPositon);
    }

    public void SetSpeed(Vector2 speed)
    {
        if (speed.y == 0)
        {
            if (speed.x != 0)
                SetAnimation(run);
            else if (speed.x == 0)
                SetAnimation(idle);
        }
        else if (speed.y < 0)
        {
            SetAnimation(fall);
        }
        else if (speed.y > 0)
            SetAnimation(jump);

        if (speed.x < 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else if (speed.x > 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void Stunned()
    {
        SetAnimation(stunned);
    }

    public void Die()
    {
        SetAnimation(die, false, 0, false);
    }

    public void Hit()
    {
        SetAnimation(hit, false, 1);
    }

    public void Shoot()
    {
        SetAnimation(shoot, false, 1);
    }

    private void SetAnimation(string state, bool loop = true, int layer = 0, bool endTrack = true)
    {
        Spine.TrackEntry entry = skeleton.AnimationState.GetCurrent(layer);
        if (entry == null || (entry.animation.Name != state))
        {
            skeleton.AnimationState.SetAnimation(layer, state, loop);
            if (!loop && endTrack)
            {
                entry = skeleton.AnimationState.GetCurrent(layer);
                entry.trackEnd = entry.animation.duration;
            }
        }
    }
}
