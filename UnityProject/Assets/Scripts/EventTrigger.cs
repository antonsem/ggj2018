using UnityEngine;

public class EventTrigger : MonoBehaviour
{
    public GameObject[] disable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 0; i < disable.Length; i++)
        {
            disable[i].SetActive(false);
        }
    }

}
