using UnityEngine;

public class CheckCollision : MonoBehaviour
{
    private bool _onTheGround;
    public bool onTheGround
    {
        get
        {
            return _onTheGround;
        }
    }

    public HitTheGround hitTheGround = new HitTheGround();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _onTheGround = true;
        hitTheGround.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _onTheGround = false;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        _onTheGround = true;
    }
}
