using UnityEngine;

public interface IBreakable
{
    void Hit(float damage, Vector3 contactPoint, Vector3 normal);
    void KickBack(Vector3 dir, float stunTime);
}
