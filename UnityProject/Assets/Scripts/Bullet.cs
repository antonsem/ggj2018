using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField]
    private Rigidbody2D rigid;
    public float speed = 10;
    private Vector3 direction;
    public float hitDamage = 10;

    private void Start()
    {
        Destroy(gameObject, 3);
    }

    public void ReverseDirection()
    {
        direction *= -1;
        ChangeDirection(direction);
    }

    public void ChangeDirection(Vector3 dir)
    {
        transform.right = -dir;
        direction = dir;
        rigid.velocity = dir * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IBreakable breakable = collision.gameObject.GetComponent<IBreakable>();
        if (breakable != null)
        {
            breakable.Hit(hitDamage, transform.position, -transform.right);
            breakable.KickBack(direction * 2 + Vector3.up * 2, 2);
        }

        Destroy(gameObject);
    }
}
