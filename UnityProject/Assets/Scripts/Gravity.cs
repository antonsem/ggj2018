using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Gravity : MonoBehaviour
{
    public float gravity = 30f;
    public float terminalVelocity = 10f;
    public bool applyGravity = true;
    private Rigidbody2D rigid;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    public void ApplyGravity()
    {
        Vector2 vel = rigid.velocity;
        if (vel.y > -terminalVelocity)
            vel.y -= gravity * Time.deltaTime;
        else
            vel.y = -terminalVelocity;
        rigid.velocity = vel;
    }


    public void Update()
    {
        if (applyGravity)
            ApplyGravity();
    }
}
