using UnityEngine;

public enum Panel
{
    Main,
    Credits,
    HowToPlay
}

public class MainMenu : MonoBehaviour
{
    public GameObject howToPlayPanel;
    public GameObject creditsPanel;
    public GameObject mainMenuPanel;

	void Start()
	{
        SwitchTo(Panel.Main);
	}

    public void SwitchTo(Panel panel)
    {
        howToPlayPanel.SetActive(panel == Panel.HowToPlay);
        creditsPanel.SetActive(panel == Panel.Credits);
        mainMenuPanel.SetActive(panel == Panel.Main);
    }
	
}
