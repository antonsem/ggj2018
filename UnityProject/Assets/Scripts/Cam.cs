using UnityEngine;

public class Cam : MonoBehaviour
{
    public Transform objToFollow;
    public Vector3 offset;
    public bool canMove = true;
    public float speed = 10;

    private void Move()
    {
        if (objToFollow)
            transform.position = Vector3.MoveTowards(transform.position, objToFollow.position + offset, 0.1f);
            //transform.position += (objToFollow.position + offset - transform.position) * Time.deltaTime * speed;
    }

    void Update()
    {
        if (canMove)
            Move();
    }
}
