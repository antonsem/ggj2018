using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour, IBreakable
{
    public Health health;
    private CheckCollision col;
    private Gravity gravity;
    private Rigidbody2D rigid;
    public float jumpSpeed = 10;
    public float jumpCost = 5;
    public float jumpPauseRegen = 2;
    public float moveSpeed = 100;
    [SerializeField]
    private AnimationController anim;
    [SerializeField]
    private GameObject attack;
    public float attackCost = 20;
    public float attackPauseRegen;
    private int groundCheckAfter = 2;
    private float stun = 0;
    private float pauseRegen = 5;
    public float regenSpeed = 10;
    public bool died = false;
    public ParticleSystem blood;

    void Start()
    {
        col = GetComponentInChildren<CheckCollision>();
        col.hitTheGround.AddListener(HitTheGroundEvent);
        rigid = GetComponent<Rigidbody2D>();
        gravity = GetComponent<Gravity>();
        attack.SetActive(false);
        EventManager.enemyDied.AddListener((Enemy) => QuickRestore(50));
    }

    void Move()
    {
        Vector3 vel = rigid.velocity;
        if (stun <= 0)
            vel.x = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
        else
        {
            if (groundCheckAfter <= 0 && col.onTheGround)
                vel.x = 0;
            else
                groundCheckAfter--;
            stun -= Time.deltaTime;
        }
        rigid.velocity = vel;
        if (Input.GetKeyDown(KeyCode.Space) && col.onTheGround && stun <= 0 && health.value > 0)
        {
            health.value -= jumpCost;
            if (pauseRegen < jumpPauseRegen)
                pauseRegen = jumpPauseRegen;
            rigid.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
        }
        if (stun <= 0)
            anim.SetSpeed(rigid.velocity);
        else
            anim.Stunned();
    }

    public void QuickRestore(float amount)
    {
        health.value += amount;
        pauseRegen = 0;
    }

    void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) && stun <= 0 && health.value > 0)
        {
            anim.Shoot();
            StartCoroutine(AttackCoroutine(0.5f));
        }
    }

    IEnumerator AttackCoroutine(float time)
    {
        if (attack.activeSelf) yield break;
        health.value -= attackCost;
        if (pauseRegen < attackPauseRegen)
            pauseRegen = attackPauseRegen;
        attack.SetActive(true);
        yield return new WaitForSeconds(time);
        attack.SetActive(false);
    }

    private void HitTheGroundEvent()
    {
        gravity.applyGravity = false;
        Vector2 vel = rigid.velocity;
        vel.y = 0;
        rigid.velocity = vel;
    }

    void Update()
    {
        gravity.applyGravity = !col.onTheGround;
        if (health.value > 0)
        {
            GetInput();
            Move();
        }
        else if (!died)
        {
            if (col.onTheGround)
            {
                anim.Stunned();
                rigid.velocity = Vector3.zero;
            }
        }
        else if (died && col.onTheGround)
            rigid.velocity = Vector3.zero;

        Regen();
    }

    public void Regen()
    {
        if (died) return;
        if (pauseRegen > 0)
            pauseRegen -= Time.deltaTime;
        else if (health.value < 100)
            health.value += Time.deltaTime * regenSpeed;
        else if (health.value > 100)
            health.value = 100;
    }

    public void Hit(float damage, Vector3 contactPoint, Vector3 normal)
    {
        blood.gameObject.transform.position = contactPoint;
        blood.gameObject.transform.up = normal;
        blood.Play();
        if (health.value > 0)
        {
            health.value -= damage;
            pauseRegen = 7;
        }
        else
        {
            anim.Die();
            died = true;
        }
    }

    public void KickBack(Vector3 dir, float stunTime)
    {
        groundCheckAfter = 2;
        rigid.velocity = Vector3.zero;
        rigid.AddForce(dir, ForceMode2D.Impulse);
        stun = stunTime / 3;
    }
}
