using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuPanel : MonoBehaviour
{
    public MainMenu ui;
    public Button newGame;
    public Button credits;
    public Button howToPlay;
    public Button quit;

    void Start()
    {
        quit.onClick.AddListener(Application.Quit);
        credits.onClick.AddListener(() => ui.SwitchTo(Panel.Credits));
        howToPlay.onClick.AddListener(() => ui.SwitchTo(Panel.HowToPlay));
        newGame.onClick.AddListener(() => SceneManager.LoadScene("Main"));
    }

}
